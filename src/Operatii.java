public class Operatii {
    public Polinom add(Polinom a, Polinom b) {
        Polinom rezultat = new Polinom();
        int ok;
        for (Monom i : a.getList()) {
            ok = 0;
            for (Monom j : b.getList()) {
                if (i.getPutere() == j.getPutere()) {
                    Monom p = new Monom(i.getPutere(), i.getCoeficient() + j.getCoeficient());
                    rezultat.add(p);
                    ok = 1;
                }
            }
            if (ok == 0)
                rezultat.add(i);
        }
        for (Monom j : b.getList()) {
            ok = 0;
            for (Monom i : rezultat.getList()) {
                if (j.getPutere() == i.getPutere())
                    ok = 1;
            }
            if (ok == 0)
                rezultat.add(j);
        }
        return rezultat;
    }

    public Polinom substract(Polinom a, Polinom b) {
        Polinom rezultat = new Polinom();
        int ok;
        for (Monom i : a.getList()) {
            ok = 0;
            for (Monom j : b.getList()) {
                if (i.getPutere() == j.getPutere()) {
                    Monom p = new Monom(i.getPutere(), i.getCoeficient() - j.getCoeficient());
                    rezultat.add(p);
                    ok = 1;
                }
            }
            if (ok == 0){
                i.setCoeficient(i.getCoeficient()*(-1));
                rezultat.add(i);
            }
        }
        for (Monom j : b.getList()) {
            ok = 0;
            for (Monom i : rezultat.getList()) {
                if (j.getPutere() == i.getPutere())
                    ok = 1;
            }
            if (ok == 0){
                j.setCoeficient(j.getCoeficient()*(-1));
                rezultat.add(j);
            }
        }
        return rezultat;
    }

    public Polinom add2(Polinom a) {
        Polinom rezultat = new Polinom();
        int ok,ok2;
        for (Monom i : a.getList()) {
            ok = 0;
            ok2 = 0;
            Monom p = new Monom(i.getPutere(),i.getCoeficient());
            for (Monom j  : a.getList()) {
                if (i.getPutere() == j.getPutere()&&i!=j) {
                    p.setCoeficient(p.getCoeficient()+j.getCoeficient());
                    ok = 1;
                }
            }
            if (ok == 0)
                rezultat.add(i);
            else{
                for(Monom q : rezultat.getList()){
                    if(p.getPutere()==q.getPutere())
                        ok2 = 1;
                }
                if(ok2==0)
                    rezultat.add(p);
            }
        }
        return rezultat;
    }

    public Polinom multiply(Polinom a, Polinom b) {
        Polinom rezultat = new Polinom();
        Polinom rezultat2 = new Polinom();
        for (Monom i : a.getList()) {
            for (Monom j : b.getList()) {
                Monom p = new Monom(i.getPutere() + j.getPutere(), i.getCoeficient() * j.getCoeficient());
                rezultat.add(p);
            }
        }
        Operatii o = new Operatii();
        rezultat2 = o.add2(rezultat);
        return rezultat2;
    }
    public Polinom derivare(Polinom a){
        Polinom rezultat = new Polinom();
        for(Monom i : a.getList()){
            if(i.getPutere()!=0) {
                Monom p = new Monom(i.getPutere() - 1, i.getCoeficient() * i.getPutere());
                rezultat.add(p);
            }
        }
        return rezultat;
    }
    public Polinom integrare(Polinom a){
        Polinom rezultat = new Polinom();
        for(Monom i : a.getList()) {
            if (i.getPutere() != 0) {
                Monom p = new Monom(i.getPutere() + 1,i.getCoeficient()/(i.getPutere()+1));
                rezultat.add(p);
            }
            else{
                Monom p = new Monom(1,i.getCoeficient());
                rezultat.add(p);
            }
        }
        return rezultat;
    }
}