import org.junit.Assert;

public class Test {

    @org.junit.Test
    public void testInput(){
        Polinom test = new Polinom();
        String s = "xxx";
        String z = "x^22x^22x^22";
        String a = "x^3+5x^2+2x+1";
        String b = "gfgdgdfg";
        String q = "x";
        String w = "+5";
        String r = "-3423423";
        String t = "x^696969";
        String o = "abc21334fdsfs";
        String p = "xx+1";
        String l = "k";
        Assert.assertEquals(test.regexP(s),null);
        Assert.assertEquals(test.regexP(z),null);
        Assert.assertNotEquals(test.regexP(a),null);
        Assert.assertEquals(test.regexP(b),null);
        Assert.assertNotEquals(test.regexP(q),null);
        Assert.assertNotEquals(test.regexP(w),null);
        Assert.assertNotEquals(test.regexP(r),null);
        Assert.assertNotEquals(test.regexP(t),null);
        Assert.assertEquals(test.regexP(o),null);
        Assert.assertEquals(test.regexP(p),null);
        Assert.assertEquals(test.regexP(l),null);
    }
    @org.junit.Test
    public void testAdunare(){
        Polinom a = new Polinom();
        Polinom b = new Polinom();
        Polinom c = new Polinom();
        Polinom d = new Polinom();
        Polinom rezultat = new Polinom();
        Polinom rezultat2 = new Polinom();
        String polinom1 = "x^2+3x+5";
        String polinom2 = "3x+2";
        String polinom3 = "x";
        String polinom4 = "+1";
        a = a.regexP(polinom1);
        b = b.regexP(polinom2);
        c = c.regexP(polinom3);
        d = d.regexP(polinom4);
        Operatii o = new Operatii();
        rezultat = o.add(a,b);
        rezultat2 = o.add(c,d);
        Assert.assertEquals(rezultat.transformare(),"Rezultatul este : 1x^2 + 6x^1 + 7x^0");
        Assert.assertEquals(rezultat2.transformare(),"Rezultatul este : 1x^1 + 1x^0");

    }
    @org.junit.Test
    public void testScadere(){
        Polinom a = new Polinom();
        Polinom b = new Polinom();
        Polinom c = new Polinom();
        Polinom d = new Polinom();
        Polinom rezultat = new Polinom();
        Polinom rezultat2 = new Polinom();
        String polinom1 = "x^2+5x+5";
        String polinom2 = "3x+2";
        String polinom3 = "x+2";
        String polinom4 = "+1";
        a = a.regexP(polinom1);
        b = b.regexP(polinom2);
        c = c.regexP(polinom3);
        d = d.regexP(polinom4);
        Operatii o = new Operatii();
        rezultat = o.substract(a,b);
        rezultat2 = o.substract(c,d);
        Assert.assertEquals(rezultat.transformare(),"Rezultatul este : 1x^2 + 2x^1 + 3x^0");
        Assert.assertEquals(rezultat2.transformare(),"Rezultatul este : 1x^1 + 1x^0");
    }
    @org.junit.Test
       public void testInmultire(){
        Polinom a = new Polinom();
        Polinom b = new Polinom();
        Polinom c = new Polinom();
        Polinom d = new Polinom();
        Polinom rezultat = new Polinom();
        Polinom rezultat2 = new Polinom();
        String polinom1 = "x^2+5x+5";
        String polinom2 = "3x+2";
        String polinom3 = "x";
        String polinom4 = "+1";
        a = a.regexP(polinom1);
        b = b.regexP(polinom2);
        c = c.regexP(polinom3);
        d = d.regexP(polinom4);
        Operatii o = new Operatii();
        rezultat = o.multiply(a,b);
        rezultat2 = o.multiply(c,d);
        Assert.assertEquals(rezultat.transformare(),"Rezultatul este : 3x^3 + 17x^2 + 25x^1 + 10x^0");
        Assert.assertEquals(rezultat2.transformare(),"Rezultatul este : 1x^1");
    }
    @org.junit.Test
    public void testDerivare(){
        Polinom a = new Polinom();
        Polinom b = new Polinom();
        Polinom rezultat = new Polinom();
        Polinom rezultat2 = new Polinom();
        String polinom1 = "x^2+5x+5";
        String polinom2 = "x";
        a = a.regexP(polinom1);
        b = b.regexP(polinom2);
        Operatii o = new Operatii();
        rezultat = o.derivare(a);
        rezultat2 = o.derivare(b);
        Assert.assertEquals(rezultat.transformare(),"Rezultatul este : 2x^1 + 5x^0");
        Assert.assertEquals(rezultat2.transformare(),"Rezultatul este : 1x^0");
    }
    @org.junit.Test
    public void testIntegrare(){
        Polinom a = new Polinom();
        Polinom b = new Polinom();
        Polinom rezultat = new Polinom();
        Polinom rezultat2 = new Polinom();
        String polinom1 = "22x^2+5x+5";
        String polinom2 = "3x";
        a = a.regexP(polinom1);
        b = b.regexP(polinom2);
        Operatii o = new Operatii();
        rezultat = o.integrare(a);
        rezultat2 = o.integrare(b);
        Assert.assertEquals(rezultat.transformare(),"Rezultatul este : 7x^3 + 2x^2 + 5x^1");
        Assert.assertEquals(rezultat2.transformare(),"Rezultatul este : 1x^2");
    }


}
