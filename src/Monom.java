public class Monom implements Comparable {
    private int coeficient;
    private int putere;
    public Monom (int putere,int coeficient){
        this.putere = putere;
        this.coeficient = coeficient;
    }
    public int getPutere(){
        return this.putere;
    }
    public int compareTo(Object other){
        Monom m = (Monom) other;
        if(this.putere > ((Monom) other).getPutere()){
            return -1;
        }
        return 1;
    }
    public int getCoeficient(){
        return this.coeficient;
    }
    public void setCoeficient(int value){
        this.coeficient = value;
    }
    public void setPutere(int value){
        this.putere = value;
    }

}
