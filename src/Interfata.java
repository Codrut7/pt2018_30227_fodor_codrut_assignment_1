import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;


public class Interfata extends Application implements EventHandler<ActionEvent> {
    Label label1 = new Label("Inserati primul polinom : ");
    Label label2 = new Label("inserati al doilea polinom : ");
    Label label3 = new Label();
    Label label4 = new Label();
    Button bt1 = new Button();
    Button bt2 = new Button();
    Button bt3 = new Button();
    Button bt4 = new Button();
    Button bt5 = new Button();
    TextField text1 = new TextField();
    TextField text2 = new TextField();

    @Override
    public void start(Stage stage) {
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));

        Scene scene = new Scene(grid, 800, 600);
        scene.setFill(Color.LIGHTGRAY);
        bt1.setText("Adunare");
        bt2.setText("Scadere");
        bt3.setText("Inmultire");
        bt4.setText("Derivare");
        bt5.setText("Integrare");
        grid.add(label1, 0, 0);
        grid.add(label2, 0, 2);
        grid.add(text1, 1, 0);
        grid.add(text2, 1, 2);
        grid.add(bt1, 0, 4);
        grid.add(bt2, 1, 4);
        grid.add(bt3, 0, 5);
        grid.add(bt4, 1, 5);
        grid.add(bt5, 1, 6);
        grid.add(label3, 1, 8);
        grid.add(label4, 1, 9);
        stage.setScene(scene);
        stage.show();
        text1.setOnAction(this);
        text2.setOnAction(this);
        bt1.setOnAction(this);
        bt2.setOnAction(this);
        bt3.setOnAction(this);
        bt4.setOnAction(this);
        bt5.setOnAction(this);

    }


    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void handle(ActionEvent event) {
        if (event.getSource() == bt1) {
            String s = text1.getText();
            String s2 = text2.getText();
            String rezultat2;
            Polinom p = new Polinom();
            Polinom q = new Polinom();
            Polinom rezultat = new Polinom();
            Operatii o = new Operatii();
            if (p.regexP(s) != null && q.regexP(s2) != null) {
                p = p.regexP(s);
                q = q.regexP(s2);
                rezultat = o.add(p, q);
                rezultat2 = rezultat.transformare();
                label3.setText(rezultat2);
                label4.setText("");
            }
            else{
                label3.setText("Ai gresit polinomul, baiatu' meu");
            }
        }
        if (event.getSource() == bt2) {
            String s = text1.getText();
            String s2 = text2.getText();
            String rezultat2;
            Polinom p = new Polinom();
            Polinom q = new Polinom();
            Polinom rezultat = new Polinom();
            Operatii o = new Operatii();
            if (p.regexP(s) != null && q.regexP(s2) != null) {
                p = p.regexP(s);
                q = q.regexP(s2);
                rezultat = o.substract(p, q);
                rezultat2 = rezultat.transformare();
                label3.setText(rezultat2);
                label4.setText("");
            }
            else{
                label3.setText("Ai gresit polinomul, baiatu' meu");
            }
        }
        if (event.getSource() == bt4) {
            String s = text1.getText();
            String s2 = text2.getText();
            String rezultatS1;
            String rezultatS2;
            Polinom p = new Polinom();
            Polinom q = new Polinom();
            Polinom rezultat1 = new Polinom();
            Polinom rezultat2 = new Polinom();
            Operatii o = new Operatii();
            if (p.regexP(s) != null && q.regexP(s2) != null) {
                p = p.regexP(s);
                q = q.regexP(s2);
                rezultat1 = o.derivare(p);
                rezultat2 = o.derivare(q);
                rezultatS1 = rezultat1.transformare();
                rezultatS2 = rezultat2.transformare();
                label3.setText(rezultatS1);
                label4.setText(rezultatS2);
            }
            else{
                label3.setText("Ai gresit polinomul, baiatu' meu");
            }
        }
        if (event.getSource() == bt3) {
            String s = text1.getText();
            String s2 = text2.getText();
            String rezultat2;
            Polinom p = new Polinom();
            Polinom q = new Polinom();
            Polinom rezultat = new Polinom();
            Operatii o = new Operatii();
            if (p.regexP(s) != null && q.regexP(s2) != null) {
                p = p.regexP(s);
                q = q.regexP(s2);
                rezultat = o.multiply(p, q);
                rezultat2 = rezultat.transformare();
                label3.setText(rezultat2);
                label4.setText("");
            }
            else{
                label3.setText("Ai gresit polinomul, baiatu' meu");
            }
        }
        if (event.getSource() == bt5) {
            String s = text1.getText();
            String s2 = text2.getText();
            String rezultatS1;
            String rezultatS2;
            Polinom p = new Polinom();
            Polinom q = new Polinom();
            Polinom rezultat1 = new Polinom();
            Polinom rezultat2 = new Polinom();
            Operatii o = new Operatii();
            if (p.regexP(s) != null && q.regexP(s2) != null) {
                p = p.regexP(s);
                q = q.regexP(s2);
                rezultat1 = o.integrare(p);
                rezultat2 = o.integrare(q);
                rezultatS1 = rezultat1.transformare();
                rezultatS2 = rezultat2.transformare();
                label3.setText(rezultatS1);
                label4.setText(rezultatS2);
            }
            else{
                label3.setText("Ai gresit polinomul, baiatu' meu");
            }
        }
    }
}