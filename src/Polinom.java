import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Polinom {
    private List<Monom> lista = new ArrayList<>();

    public void add(Monom m) {
        lista.add(m);
    }

    public void remove(int i) {
        lista.remove(i);
    }

    public List<Monom> getList() {
        return this.lista;
    }

    public void afisare() {
        Collections.sort(lista);
        for (Monom i : this.lista) {
            System.out.print(i.getCoeficient() + "x ^ " + i.getPutere() + " + ");
        }
    }

    public String transformare(){
        String s = "Rezultatul este : ";
        int count = 0;
        for(Monom i : this.lista){
            if(i.getCoeficient()>0&&count==0){
                s = s + i.getCoeficient() + "x^" + i.getPutere();
            }
            else if(i.getCoeficient()>0&&count>0){
                s = s + " + " + i.getCoeficient() + "x^" + i.getPutere();
            }
            else{
                s = s + i.getCoeficient() + "x^" + i.getPutere();
            }
            count++;
        }
            return s;
    }


    public Polinom regexP(String s) {
        Polinom rezultat = new Polinom();
        String cautare1 = "([+-]?\\d*)x(\\^\\d*)?|([+-]\\d*)";
        Pattern check = Pattern.compile(cautare1);
        Matcher regexMatcher = check.matcher(s);
        int flagCifra = 0;
        int flagInceput = 0;
        while (regexMatcher.find()) {
            Monom p = new Monom(0, 0);
            String coeficient = regexMatcher.group(1);
            String putere = regexMatcher.group(2);
             if(coeficient==null){
                p.setCoeficient(Integer.parseInt(regexMatcher.group(3)));
                flagCifra = 1;
            }
            else if(coeficient.equals("")&&flagInceput==0){
                 p.setCoeficient(1);
            }
            else if(coeficient.equals("")&&flagInceput!=0){
                 rezultat = null;
                 break;
             }
            else if(coeficient.charAt(0)!='+'&&coeficient.charAt(0)!='-'){
                 p.setCoeficient(Integer.parseInt(coeficient));
             }
            else if(coeficient.charAt(0)=='+'&&coeficient.length()>1){
                p.setCoeficient(Integer.parseInt(coeficient.substring(coeficient.lastIndexOf('+')+1)));
            }
            else if(coeficient.charAt(0)=='-'&&coeficient.length()>1){
                p.setCoeficient((-1)*Integer.parseInt(coeficient.substring(coeficient.lastIndexOf('-')+1)));
            }
            else if(coeficient.equals("-")){
                p.setCoeficient(-1);
            }
            else{
                p.setCoeficient(1);
            }
            if(putere==null&&flagCifra==0) {
                p.setPutere(1);
            }
            else if (putere==null&&flagCifra==1){
                 p.setPutere(0);
            }
            else if(putere.charAt(0)=='^'){
                p.setPutere(Integer.parseInt(putere.substring(putere.lastIndexOf('^')+1)));
            }
            rezultat.add(p);
             flagInceput = 1;
        }
          if(flagInceput==0){
            rezultat = null;
          }
          return rezultat;
    }
}